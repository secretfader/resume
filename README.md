# Resume

[![CI Status
Badge](https://gitlab.com/secretfader/resume/badges/master/build.svg)](https://gitlab.com/secretfader/resume)
[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

The resume of hacker and artist Nicholas Young (a.k.a. "Fader").

## License

Copyright 2019 Nicholas Young. All rights reserved.

Markup and design may be used under the [3-Clause BSD License](LICENSE). However, republishing content without express permission is a violation of applicable laws.
